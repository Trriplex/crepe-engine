CFLAGS = -std=c99 -Wall
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf
CC = gcc

demo:
	$(CC) -o demo demo.c $(CFLAGS) $(LIBS)
	./demo

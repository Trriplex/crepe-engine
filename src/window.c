SDL_Window* win;
SDL_Renderer* ren;

void init(char* title, int w, int h) {
	SDL_Init(SDL_INIT_VIDEO);
	IMG_Init(IMG_INIT_PNG);
	TTF_Init();

	win = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, w, h,
			0);
	ren = SDL_CreateRenderer(win, -1, 0);
}

void quit(void) {
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

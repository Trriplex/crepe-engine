typedef struct sprite {
	SDL_Rect pos;
	SDL_Texture *t;
} sprite;

void loadSprite(sprite* sp, char* path) {
	sp->t = IMG_LoadTexture(ren, path);
	sp->pos.x = 0;
	sp->pos.y = 0;
	SDL_QueryTexture(sp->t, NULL, NULL, &sp->pos.w, &sp->pos.h);
}

void draw(sprite sp) {
	SDL_RenderCopy(ren, sp.t, NULL, &sp.pos);
}

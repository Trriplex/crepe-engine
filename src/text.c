typedef TTF_Font* font;

void loadFont(font* f, const char* path, int size) {
	*f = TTF_OpenFont(path, size);
}

void loadText(sprite* t, font f, const char* string, int r, int g, int b) {
	SDL_Color color = {r, g, b};
	SDL_Rect pos = {0, 0, 100, 100};
	SDL_Surface* s = TTF_RenderText_Solid(f, string, color);
	t->pos = pos;
	t->t = SDL_CreateTextureFromSurface(ren, s);
	SDL_FreeSurface(s);
}

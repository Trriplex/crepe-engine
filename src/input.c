typedef struct kbState {
	bool z;
	bool q;
	bool s;
	bool d;
} kbState;

void loadKb(kbState* kb) {
	*kb = (kbState){false, false, false, false};
}

void handleInput(kbState* kb, bool* c) {
	SDL_Event e;
	if (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			*c = false;
		} else if (e.type == SDL_KEYDOWN) {
			switch (e.key.keysym.sym) {
				case SDLK_ESCAPE:
					*c = false;
					break;
				case SDLK_z:
					kb->z = true;
					break;
				case SDLK_q:
					kb->q = true;
					break;
				case SDLK_s:
					kb->s = true;
					break;
				case SDLK_d:
					kb->d = true;
					break;
			}
		} else if (e.type == SDL_KEYUP) {
			switch (e.key.keysym.sym) {
				case SDLK_z:
					kb->z = false;
					break;
				case SDLK_q:
					kb->q = false;
					break;
				case SDLK_s:
					kb->s = false;
					break;
				case SDLK_d:
					kb->d = false;
					break;
			}
		}
	}
}

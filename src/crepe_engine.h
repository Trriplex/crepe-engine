#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#define clear() SDL_RenderClear(ren)
#define wait(time) SDL_Delay(time)
#define render() SDL_RenderPresent(ren)
#include "window.c"
#include "sprite.c"
#include "input.c"
#include "text.c"

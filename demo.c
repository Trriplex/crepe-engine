#include "src/crepe_engine.h"
#include <stdlib.h>
#include <time.h>  

int main(void) {
	init("krampouezh Game", 640, 480);

	srand(time(0));

	font dejavu;
	loadFont(&dejavu, "assets/dejavu.ttf", 100);

	char score_string[4] = "0";
	sprite score_text;
	loadText(&score_text, dejavu, score_string, 255, 255, 255);

	sprite crepe;
	sprite poele;
	sprite bg;
	loadSprite(&bg, "assets/bg.png");
	loadSprite(&poele, "assets/poele.png");
	loadSprite(&crepe, "assets/crepe.png");

	poele.pos.y = 340;
	int poele_width = poele.pos.w;
	int crepe_width = crepe.pos.w;
	crepe.pos.x = rand() % (640-crepe_width);

	bool c = true;
	kbState kb;
	loadKb(&kb);

	int speed = 8;
	int crepe_speed = 4;
	int score = 0;

	while (c) {
		handleInput(&kb, &c);

		if (kb.q && poele.pos.x > 0)
			poele.pos.x -= speed;
		if (kb.d && poele.pos.x < 640 - poele_width)
			poele.pos.x += speed;

		if (crepe.pos.y < 335) {
			crepe.pos.y += crepe_speed;
		} else {
			if (crepe.pos.x > poele.pos.x &&
				crepe.pos.x + crepe_width < poele.pos.x+poele_width) {
				score++;
				sprintf(score_string, "%d", score);
				loadText(&score_text, dejavu, score_string, 255, 255, 255);
				if (score % 10 == 0) {
					crepe_speed *= 2;
					speed *= 2;
				}
				wait(500);
				crepe.pos.y = -crepe.pos.h;
				crepe.pos.x = rand() % (640-crepe_width);
			} else {
				sprite perdu;
				loadText(&perdu, dejavu, "PERDU !!!!", 255, 0, 0);
				perdu.pos.w = 450;
				perdu.pos.h = 150;
				perdu.pos.y = 150;
				perdu.pos.x = (640/2)-(450/2);
				draw(perdu);
				render();
				wait(1500);
				c = false;
			}
		}

		clear();
		draw(bg);
		draw(poele);
		draw(crepe);
		draw(score_text);
		render();
		wait(16);
	}
	SDL_DestroyTexture(score_text.t);
	quit();
	return 0;
}
